export const setsEvents = {
  setA: new Set(['KeyA']),
  setD: new Set(['KeyD']),
  setJ: new Set(['KeyJ']),
  setL: new Set(['KeyL']),
  setQWE: new Set(['KeyQ', 'KeyW', 'KeyE']),
  setUIO: new Set(['KeyU', 'KeyI', 'KeyO']),
}

export const setsDoNothing = {
  setAD: new Set(['KeyA', 'KeyD']),
  setJL: new Set(['KeyJ','KeyL']),
  setAL: new Set(['KeyA','KeyL']),
  setJD: new Set(['KeyJ','KeyD']),
}
