import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if ( fighter ) {
    let nameElement = createElement({
      tagName: 'span',
      className: 'fighter-preview___name',
    });
    nameElement.textContent = fighter.name;

    let imageElement = createFighterImage(fighter);
    imageElement.classList.add('fighter-preview___image');

    let detailsElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___details',
    });
    detailsElement.innerHTML = 
    `<ul>
      <li>Attack: ${fighter.attack}</li>
      <li>Defense: ${fighter.defense}</li>
      <li>Health: ${fighter.health}</li>
    </ul>`;
    
    fighterElement.append(nameElement, imageElement, detailsElement);
  }

  return fighterElement; 
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
