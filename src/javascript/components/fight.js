import { controls } from '../../constants/controls';
import { setsEvents, setsDoNothing } from '../../constants/gameEvents';

export async function fight(firstFighter, secondFighter) {
  let pressedKeys = new Set();
  let firstFighterAttack;
  let secondFighterAttack;
  document.addEventListener( 'keydown', function(event) {
    pressedKeys.add(event.code);

    if (event.code === controls.PlayerOneAttack || event.code === controls.PlayerTwoAttack) {
      
      if (!(checkIfEqual(pressedKeys, setsDoNothing.setAD) ||
        checkIfEqual(pressedKeys, setsDoNothing.setAL) || 
        checkIfEqual(pressedKeys, setsDoNothing.setJD) ||
        checkIfEqual(pressedKeys, setsDoNothing.setJL) )) {
          
        if (event.code === controls.PlayerOneAttack) {
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          showHealthBar(secondFighter);
        }
        if (event.code === controls.PlayerTwoAttack) {
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          showHealthBar(firstFighter);
        }
      }
    } else if (checkIfEqual(pressedKeys, setsEvents.setQWE)) {
      if (!firstFighterAttack) {
        firstFighterAttack = true;
        secondFighter.health -= 2 * firstFighter.attack;
        showHealthBar(secondFighter);
        setTimeout(() => {
          firstFighterAttack = false;
        }, 10000);
      } 
    } else if (checkIfEqual(pressedKeys, setsEvents.setUIO)) {
      if (!secondFighterAttack) {
        secondFighterAttack = true;
        firstFighter.health -= 2 * secondFighter.attack;
        showHealthBar(firstFighter);
        setTimeout(() => {
          secondFighterAttack = false;
        }, 10000);
      } 
    }

    return new Promise((resolve) => {
      if (firstFighter.health === 0) {
        resolve(secondFighter);
      } 
      if (secondFighter.health === 0) {
        resolve(firstFighter);
      }
    });
  });

  document.addEventListener('keyup', function(event) {
    pressedKeys.delete(event.code);
  });
}

function checkIfEqual (firstSet, secondSet) {
  if (firstSet.size !== secondSet.size) {
    return false;
  }
  for (let value of firstSet) {
    if (!secondSet.has(value)) {
      return false;
    }
  }
  return true;
}

function showHealthBar(fighter) {
  // TODO
  // console.log('health ...',fighter.health);
}

function randomValue() {
  return Math.random() + 1;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage < 0) ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = randomValue();
  const hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomValue();
  const blockPower = fighter.defender * dodgeChance;
  return blockPower;
}
