import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  let imageWinnerElement = createFighterImage(fighter);
  showModal({
    title: `The winner is ${fighter.name}!`,
    bodyElement: imageWinnerElement,
  });
}
